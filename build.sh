#!/bin/bash
set -e

pushd `dirname $0` > /dev/null

image='tadams/graphviz:latest'
docker build -t $image . \
	&& docker login \
	&& docker push $image
