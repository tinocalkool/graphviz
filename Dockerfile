FROM ubuntu:trusty

# Install prerequisites
RUN apt-get update
RUN apt-get install -y graphviz

VOLUME /workspace
WORKDIR /workspace

ENTRYPOINT [ "/usr/bin/dot" ]